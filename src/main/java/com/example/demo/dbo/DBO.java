package com.example.demo.dbo;

/**
 * Marker interface for DBOs.
 *
 * @author Daniel Westhoff
 * @since 1.1.0
 */
public interface DBO {

    /**
     * Returns the DB identifier of the DBO.
     *
     * @return the identifier
     */
    Long getId();

}
