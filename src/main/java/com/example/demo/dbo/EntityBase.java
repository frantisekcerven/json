package com.example.demo.dbo;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base class for all entities.
 *
 * @author Daniel Westhoff
 * @since 1.1.0
 */
@MappedSuperclass
@Getter
public abstract class EntityBase implements DBO, Identifiable<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private Long id;

}
