package com.example.demo.dbo;

import java.time.LocalDateTime;

/**
 * Marks auditable classes.
 *
 * @author Daniel Westhoff
 * @since 1.1.0
 */
public interface Auditable {

    /**
     * Returns the date and time when the object was created.
     *
     * @return date and time when the object was created.
     */
    LocalDateTime getCreated();

    /**
     * Returns the date and time when the object was last modified.
     *
     * @return date and time when the object was last modified.
     */
    LocalDateTime getLastModified();

}
