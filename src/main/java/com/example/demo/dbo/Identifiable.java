package com.example.demo.dbo;

/**
 * Marks identifiable classes.
 *
 * @author Daniel Westhoff
 * @since 1.1.0
 */
public interface Identifiable<T> {

    /**
     * Returns the unique identifier of the object.
     *
     * @return the unique identifier
     */
    T getId();

}
