package com.example.demo.dbo;

import lombok.Getter;
import lombok.Setter;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A user role used to grant access to endpoints.
 *
 * @author Daniel Westhoff
 * @since 0.1.0
 */
@Entity
@Table(name = "api_transaction")
@Getter
@Setter
public class TransactionDbo extends EntityBaseAudit {

    @Column(name = "caller", nullable = false)
    String caller;

    @Column(name = "clientuser", nullable = false)
    String clientUser;

    @Column (nullable = true, columnDefinition = "jsonb")
    private JSONObject data;
}
