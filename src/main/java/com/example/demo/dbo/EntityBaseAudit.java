package com.example.demo.dbo;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * Base class for entities that track creation and modification dates.
 *
 * @author Daniel Westhoff
 * @since 1.1.0
 */
@MappedSuperclass
@Getter
public abstract class EntityBaseAudit extends EntityBase implements Auditable {

    @Column(name = "created", nullable = false, updatable = false)
    private LocalDateTime created;

    @Column(name = "lastmodified", nullable = false)
    private LocalDateTime lastModified;

}
